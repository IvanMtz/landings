const crypto = require('crypto');
const request = require('request');
const nodemailer = require('nodemailer');

const LandingModel = require('../models/landingConfig.model');
const smptConfig = require('../../common/config/smpt.config.json');

async function sendEmail(email) {
    const transport = nodemailer.createTransport({
        host: smptConfig.host,
        port: smptConfig.port,
        auth: {
            user: smptConfig.user,
            pass: smptConfig.password,
        },
    });

    const message = {
        from: 'Hola@jesusnava.mx',
        to: 'Hola@jesusnava.mx',
        subject: 'Hola',
        html: `${email} <br /> Correo automático, no responder.`,
    };

    transport.sendMail(message, function (err, info) {
        if (err) {
            console.log('err', err)
        } else {
            console.log('info', info);
        }
    });
}

exports.saveLandingFrom = async (req, res) => {
    let body = {};
    const { email, id, inputValues, hook } = req.body;

    sendEmail(email);

    if (!hook) {
        res.status(204).send({});

        return;
    }

    const landing = await LandingModel.findById(id);

    for (let input of inputValues) {
        body[input.id] = input.value;
    }

    try {
        const options = {
            method: 'POST',
            url: landing.hook,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body)
        };

        request(options, function (error, response) {
            if (error) {
                res.status(500).send({ error });
            } else {
                res.status(200).send({ email: 'jeje' });
            }
        });
    } catch (error) {
        res.status(500).send({ error });
    }
}

exports.insert = (req, res) => {
    LandingModel.createLandig(req.body)
        .then((result) => {
            res.status(200).send({ id: result._id });
        });
};

exports.list = (req, res) => {
    let limit = req.query.limit && req.query.limit <= 100 ? parseInt(req.query.limit) : 10;
    let page = 0;
    if (req.query) {
        if (req.query.page) {
            req.query.page = parseInt(req.query.page);
            page = Number.isInteger(req.query.page) ? req.query.page : 0;
        }
    }
    LandingModel.list(limit, page)
        .then((result) => {
            res.status(200).send(result);
        })
};

exports.getByUrl = (req, res) => {
    LandingModel.findByUrl(req.params.url)
        .then((result) => {
            res.status(200).send(result);
        });
};

exports.patchById = (req, res) => {
    if (req.body.password) {
        let salt = crypto.randomBytes(16).toString('base64');
        let hash = crypto.createHmac('sha512', salt).update(req.body.password).digest("base64");
        req.body.password = salt + "$" + hash;
    }

    LandingModel.patchLandingConfig(req.params.id, req.body)
        .then((result) => {
            res.status(204).send({});
        });

};

exports.removeById = (req, res) => {
    LandingModel.removeById(req.params.id)
        .then((result) => {
            res.status(204).send({});
        });
};