const LandingsController = require('./controllers/landingConfig.controller');
const ValidationMiddleware = require('../common/middlewares/auth.validation.middleware');
const config = require('../common/config/env.config');

exports.routesConfig = function (app) {
    app.post('/landingsConfig', [
        ValidationMiddleware.validJWTNeeded,
        LandingsController.insert,
    ]);
    app.get('/landingsConfig', [
        ValidationMiddleware.validJWTNeeded,
        LandingsController.list,
    ]);
    app.get('/landingsConfig/:url', [
        ValidationMiddleware.validJWTNeeded,
        LandingsController.getByUrl,
    ]);
    app.delete('/landingsConfig/:id', [
        ValidationMiddleware.validJWTNeeded,
        LandingsController.removeById
    ]);
    app.post('/landings', [
        ValidationMiddleware.validJWTNeeded,
        LandingsController.saveLandingFrom,
    ]);
};
