const mongoose = require('../../common/services/mongoose.service').mongoose;
const Schema = mongoose.Schema;

const landingConfigSchema = new Schema({
    description: String,
    email: String,
    hook: String,
    inputs: Array,
    name: String,
    title: String,
    thanksSubTitle: String,
    thanksTitle: String,
    url: String,
    videoUrl: String,
});

landingConfigSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

// Ensure virtual fields are serialised.
landingConfigSchema.set('toJSON', {
    virtuals: true
});

landingConfigSchema.findById = function (cb) {
    return this.model('landingConfigs').find({ id: this.id }, cb);
};

const LandingConfig = mongoose.model('landingConfigs', landingConfigSchema);

exports.findByUrl = (url) => {
    return LandingConfig.find({ url: `/${url}` });
};

exports.findById = (id) => {
    return LandingConfig.findById(id)
        .then((result) => {
            result = result.toJSON();
            delete result._id;
            delete result.__v;
            return result;
        });
};

exports.createLandig = (landingConfigData) => {
    const landingConfig = new LandingConfig(landingConfigData);
    return landingConfig.save();
};

exports.list = (perPage, page) => {
    return new Promise((resolve, reject) => {
        LandingConfig.find()
            .limit(perPage)
            .skip(perPage * page)
            .exec(function (err, landingConfig) {
                if (err) {
                    reject(err);
                } else {
                    resolve(landingConfig);
                }
            })
    });
};

exports.patchLandingConfig = (id, landingConfigData) => {
    return new Promise((resolve, reject) => {
        LandingConfig.findById(id, function (err, landingConfig) {
            if (err) reject(err);
            for (let i in landingConfigData) {
                landingConfig[i] = landingConfigData[i];
            }
            landingConfig.save(function (err, updatedLandingConfig) {
                if (err) return reject(err);
                resolve(updatedLandingConfig);
            });
        });
    })
};

exports.removeById = (landingConfigId) => {
    return new Promise((resolve, reject) => {
        LandingConfig.remove({ _id: landingConfigId }, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(err);
            }
        });
    });
};
