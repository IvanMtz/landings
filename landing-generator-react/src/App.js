import React, { useContext } from 'react';
import {
  Redirect,
  Route,
  Switch,
  useHistory,
  useLocation,
} from 'react-router-dom';

import { Context } from './Context';

import { setContext } from './utils/api';

import {
  CreateLadingScreen,
  HomeScreen,
  LadingScreen,
  LadingTestScreen,
  LoginScreen,
} from './screens';

import './index.css';
import './color.css';
import { LandingThanksScreen } from './screens/LandingThanksScreen';
import { LandingThanksTestScreen } from './screens/LandingThanksTestScreen';

const App = () => {
  const location = useLocation();
  const history = useHistory();
  const { isAuth, removeAuth } = useContext(Context);

  setContext({ removeAuth });

  const PrivateRoutes = () => (
    <>
      <Redirect from="/home" to="/" />
      <Redirect from="/create-landing" to="/" />
    </>
  );

  return (
    <>
      <Switch>
        <Route exact path="/">
          {isAuth ? <HomeScreen /> : <LoginScreen />}
        </Route>

        {isAuth ? null : <PrivateRoutes />}

        <Route path="/home">
          <HomeScreen />
        </Route>

        <Route exact path="/create-landing">
          <CreateLadingScreen />
        </Route>

        <Route path="/landing/">
          <LadingScreen history={history} location={location} />
        </Route>

        <Route path="/landing-test">
          <LadingTestScreen location={location} />
        </Route>

        <Route path="/landing-thanks">
          <LandingThanksScreen history={history} location={location} />
        </Route>

        <Route path="/landing-test-thanks">
          <LandingThanksTestScreen history={history} location={location} />
        </Route>

        <Route>
          <h1>404</h1>
        </Route>
      </Switch>
    </>
  );
};

export { App };
