import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import './index.css';
import ContextRoot from './Context';
import { App } from './App';

ReactDOM.render(
  <React.StrictMode>
    <ContextRoot.Provider>
      <Router basename="/">
        <App />
      </Router>
    </ContextRoot.Provider>
  </React.StrictMode>,
  document.getElementById('root'),
);
