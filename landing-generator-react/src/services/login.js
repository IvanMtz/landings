import { post } from '../utils/api';

export async function login({ callback, params }) {
  const response = await post('auth', params);

  if (callback && typeof callback === 'function') {
    callback(response);
  }
}
