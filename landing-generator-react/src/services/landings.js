import { post, get, del } from '../utils/api';

export async function getLandingList(callback) {
  const response = await get('landingsConfig');

  if (callback && typeof callback === 'function') {
    callback(response);
  }
}

export async function deleteLandingById({ callback, id }) {
  const response = await del(`landingsConfig/${id}`);

  if (callback && typeof callback === 'function') {
    callback(response);
  }
}

export async function saveLandingList({ callback, data }) {
  const response = await post('landingsConfig', data);

  if (callback && typeof callback === 'function') {
    callback(response);
  }
}

export async function getLandingByUrl({ callback, url }) {
  const response = await get(`landingsConfig/${url}`);

  if (callback && typeof callback === 'function') {
    callback(response);
  }
}

export async function sendEmail({ callback, body }) {
  const response = await post('landings', body);

  if (callback && typeof callback === 'function') {
    callback(response);
  }
}
