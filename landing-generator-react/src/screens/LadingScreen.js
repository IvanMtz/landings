import React from 'react';

import {
  LandingContainer,
  LandingFooter,
  LandingHeader,
} from '../components/landing';
import { Loading, ModalContainer } from '../components/common';

import { getLandingByUrl } from '../services';

import '../components/landing/landing.css';

class LadingScreen extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = { data: { inputs: [] }, loading: true };
  }

  componentDidMount() {
    const { pathname } = this.props.location;
    const landingUrl = pathname.split('/').reverse()[0];

    if (landingUrl) {
      getLandingByUrl({ callback: this.OnGetLandingByUrl, url: landingUrl });
    }
  }

  OnGetLandingByUrl = (response) => {
    if (response.isSuccess) {
      const [newData] = response.data;

      if (newData) {
        this.setState({
          data: newData,
          loading: false,
        });

        return;
      }

      this.props.history.push('/404');
    }
  };

  render() {
    return (
      <>
        <ModalContainer show={this.state.loading} title="Cargando...">
          <Loading />
        </ModalContainer>

        <div className="primary-color-dark auto-height">
          <div className="container">
            <LandingHeader />

            <LandingContainer data={this.state.data} />
          </div>
        </div>

        <LandingFooter />
      </>
    );
  }
}

export { LadingScreen };
