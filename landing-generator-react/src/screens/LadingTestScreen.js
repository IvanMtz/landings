import React from 'react';

import {
  LandingContainer,
  LandingFooter,
  LandingHeader,
} from '../components/landing';

import '../components/landing/landing.css';
import { dataFromUrlBase64 } from '../utils';

class LadingTestScreen extends React.PureComponent {
  constructor(props) {
    super(props);

    const search = this.props.location.search || '';
    const base64 = dataFromUrlBase64(search);

    this.data = JSON.parse(base64);
  }

  render() {
    return (
      <>
        <div className="primary-color-dark auto-height">
          <div className="container">
            <LandingHeader />

            <LandingContainer isTest={true} data={this.data} />
          </div>
        </div>

        <LandingFooter />
      </>
    );
  }
}

export { LadingTestScreen };
