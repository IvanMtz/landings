import React from 'react';

import { LandingHeader } from '../components/landing/LandingHeader';
import { LandingsThanksContainer }
  from '../components/landingThanks/LandingsThanksContainer';
import { THANKS_SUB_TIELE, THANKS_TITLE } from '../components/createLanding/defaults';
import { Loading, ModalContainer } from '../components/common';

import { getLandingByUrl } from '../services';

import '../components/landingThanks/landingThanksScreen.css';

const DEFAULT_DATA = {
  thanksSubTitle: THANKS_SUB_TIELE,
  thanksTitle: THANKS_TITLE,
}

class LandingThanksScreen extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = { data: DEFAULT_DATA, loading: true };
    this.minHeight = window.innerHeight;
  }

  componentDidMount() {
    const { pathname } = this.props.location;
    const landingUrl = pathname.split('/').reverse()[0];

    if (landingUrl) {
      getLandingByUrl({ callback: this.OnGetLandingByUrl, url: landingUrl });
    }
  }

  OnGetLandingByUrl = (response) => {
    if (response.isSuccess) {
      const [newData] = response.data;

      if (newData) {
        const { thanksTitle, thanksSubTitle } = newData;

        this.setState({
          data: {
            thanksTitle: thanksTitle || DEFAULT_DATA.thanksTitle,
            thanksSubTitle: thanksSubTitle || DEFAULT_DATA.thanksSubTitle,
          },
          loading: false,
        });

        return;
      } else {
        this.props.history.push('/404');
      }
    } else {
      this.setState({ loading: false });
    }
  };

  render() {
    return (
      <>
        <ModalContainer show={this.state.loading} title="Cargando...">
          <Loading />
        </ModalContainer>

        <div
          className="primary-color-dark"
          style={{ minHeight: this.minHeight }}
        >
          <div className="container full-height col">
            <LandingHeader />

            <LandingsThanksContainer data={this.state.data} />
          </div>
        </div>
      </>
    );
  }
}

export { LandingThanksScreen };
