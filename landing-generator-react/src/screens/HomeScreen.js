import React from 'react';

import { Header } from '../components/common';
import { HomeContent } from '../components/home/HomeContent';

class HomeScreen extends React.PureComponent {
  render() {
    return (
      <>
        <Header />

        <div className="container home-container">
          <HomeContent />
        </div>
      </>
    );
  }
}

export { HomeScreen };
