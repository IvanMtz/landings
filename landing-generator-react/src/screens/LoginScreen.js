import React from 'react';

import { LoginFrom } from '../components/login/LoginFrom';

class LoginScreen extends React.PureComponent {
  render() {
    return (
      <div className="primary-color-dark login-background">
        <div className="container center full-height">
          <LoginFrom />
        </div>
      </div>
    );
  }
}

export { LoginScreen };
