import React from 'react';

import { LandingHeader } from '../components/landing/LandingHeader';
import { LandingsThanksContainer }
  from '../components/landingThanks/LandingsThanksContainer';

import '../components/landingThanks/landingThanksScreen.css';
import { dataFromUrlBase64 } from '../utils';

class LandingThanksTestScreen extends React.PureComponent {
  constructor(props) {
    super(props);

    const search = this.props.location.search || '';
    const base64 = dataFromUrlBase64(search);

    this.minHeight = window.innerHeight;
    this.data = JSON.parse(base64);
  }

  render() {
    return (
      <div
        className="primary-color-dark"
        style={{ minHeight: this.minHeight }}
      >
        <div className="container full-height col">
          <LandingHeader />

          <LandingsThanksContainer data={this.data} />
        </div>
      </div>
    );
  }
}

export { LandingThanksTestScreen };
