import React from 'react';
import {
  CreateLandingsContent,
} from '../components/createLanding/CreateLandingsContent';

import '../components/createLanding/createLanding.css';

import { Header } from '../components/common';

class CreateLadingScreen extends React.PureComponent {
  render() {
    return (
      <div className="col grey">
        <Header />

        <div className="container">
          <CreateLandingsContent />
        </div>
      </div>
    );
  }
}

export { CreateLadingScreen };
