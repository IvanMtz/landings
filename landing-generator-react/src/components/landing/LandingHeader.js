import React from 'react';

import logo from '../../assets/logo-nava-blanco.png';

const LandingHeader = () => (
  <header className="landing-header">
    <div className="landing-header-image-container">
      <img alt="jesus-nava" src={logo} />
    </div>
  </header>
);

export { LandingHeader };
