import React from 'react';

import whatsapp from '../../assets/whatsapp.png';

const WhatsappContact = () => (
  <>
    <div className="col footer-whatsapp no-mobile">
      <div className="row center">
        <a href="https://www.instagram.com/jesusnavanl/">
          <img alt="whatsapp" src={whatsapp} />
        </a>

        <span className="text-white">
          Si prefieres, envía un mensaje al 8184622514
        </span>
      </div>
    </div>

    <div className="col text-center mobile">
      <span className="text-white font-24">
        Si prefieres, envía un mensaje al
        <a className="text-white" href="tel:8184622514">
          {' 8184622514'}
        </a>
      </span>

      <span className="text-white thank-text-margin">
        ME ENCUENTRO A TU DISPOSICIÓN PARA CUALQUIER DUDA O COMENTARIO.
      </span>
    </div>
  </>
);

export { WhatsappContact };
