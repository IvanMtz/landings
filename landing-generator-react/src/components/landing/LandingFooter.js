import React, { useState } from 'react';
import jesusNava from '../../assets/jesus-nava.png';
import facebook from '../../assets/facebook.png';
import instagram from '../../assets/instagram.png';
import tweteer from '../../assets/tweteer.png';
import { ModalContainer } from '../common';
import { PolicyContainer } from './PolicyContainer';
import { WhatsappContact } from './WhatsappContact';

const LandingFooter = () => {
  const [showModal, setModal] = useState(false);

  function closeModal() {
    setModal(false);
  }

  function onOpenModal() {
    setModal(true);
  }

  return (
    <>
      <ModalContainer
        handleOutClick={closeModal}
        show={showModal}
        title="POLÍTICA DE PRIVACIDAD"
      >
        <PolicyContainer />
      </ModalContainer>

      <div className="footer-landing primary-color-dark">
        <footer>
          <div className="col center">
            <img
              alt="jesus-nava-foto"
              className="footer-jesus-nava-img"
              src={jesusNava}
            />

            <span className="text-white text-center">
              SIGUE PENDIENTE DE MIS REDES SOCIALES:
          </span>

            <div className="row center">
              <div className="footer-social-icons">
                <a href="https://www.facebook.com/jesusnavanl/">
                  <img alt="facebook" src={facebook} />
                </a>
              </div>

              <div className="footer-social-icons">
                <a href="https://www.instagram.com/jesusnavanl/">
                  <img alt="tweteer" src={instagram} />
                </a>
              </div>

              <div className="footer-social-icons">
                <a href="https://twitter.com/JesusNavaNL">
                  <img alt="instagram" src={tweteer} />
                </a>
              </div>
            </div>

            <WhatsappContact />
          </div>

        </footer>

        <div className="row blue-dark landing-policy">
          <button
            id="btn-policy"
            className="btn btn-colored-blue"
            onClick={onOpenModal}
          >
            Politica de privacidad
        </button>

          <a className="text-link" href="http://www.5merca.com/">
            Diseño por 5Merca
          </a>
        </div>
      </div>
    </>
  );
};

export { LandingFooter };
