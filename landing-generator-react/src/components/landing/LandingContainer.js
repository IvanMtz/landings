import React, { useEffect, useState, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { sendEmail } from '../../services';

import { Input, ModalContainer, Loading } from '../common';
import { toBase64 } from '../../utils';

const LandingContainer = ({ data, isTest = false }) => {
  const history = useHistory();
  const [inputValues, setInputValues] = useState([]);
  const [loading, setLoading] = useState(false);
  const [emailSended, setEmailSended] = useState(false);
  const description = data.description || '';

  useEffect(() => {
    setInputValues(data.inputs.map((item) => ({
      ...item,
      error: '',
    })));
  }, [data]);

  const OnSendEmail = useCallback((response) => {
    if (response.isSuccess) {
      setLoading(false);
      setInputValues(data.inputs);

      let url = `/landing-thanks${data.url}`;

      if (isTest) {
        url = `/landing-test-thanks?data=${toBase64(data)}`;
      }

      history.push(url);
    }
  }, [data, setLoading]);

  function setInputValue(event) {
    const { target } = event;
    setInputValues(inputValues.map((item) => {
      if (item.id === target.id) {
        return {
          ...item,
          value: target.value,
        };
      }

      return {
        ...item,
      };
    }));
  }

  function setInputError({ error, id }) {
    setInputValues(inputValues.map((item) => {
      if (item.id === id) {
        return {
          ...item,
          error,
        };
      }

      return {
        ...item,
      };
    }));
  }

  function getInputValue(id) {
    const input = inputValues.find((item) => item.id === id);

    return input ? input.value : '';
  }

  function onSubmit(event) {
    event.preventDefault();

    const formatedEmail = inputValues.reduce((prev, next) => prev
      .replace(`{${next.id}}`, next.value), data.email);
    const body = { email: formatedEmail, id: data.id, inputValues };

    setLoading(true);
    sendEmail({ callback: OnSendEmail, body });
  }

  return (
    <>
      <ModalContainer show={loading} title="Enviando...">
        <Loading />
      </ModalContainer>

      <ModalContainer
        show={emailSended}
        title="Guardado con éxito"
        handleOutClick={() => setEmailSended(false)}
      >
        <div className="col center landing-modal-success">
          <p>¡Se ha enviado tu formulario con éxito!</p>
        </div>
      </ModalContainer>

      <div className="row mobile-col">
        <div className="col landing-description-container mobile-center">
          <div className="landing-text-title text-white">
            {data.videoUrl &&
              (<iframe
                width="400px"
                height="250px"
                src={data.videoUrl}
                frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen />)}

            <div className="text-title">
              <h1 className="text-blue">{data.title}</h1>

              <span
                className="text-white"
                dangerouslySetInnerHTML={{ __html: description }}
              />
            </div>
          </div>
        </div>

        <div className="col landing-form-container">
          <div className="card landing-card">
            <form onSubmit={onSubmit}>
              <h2 className="text-center text-bold landing-card-header">
                Ingresa tus datos y me pondré en contacto
              </h2>

              {inputValues.map((item, index) => (
                <Input
                  key={String(index)}
                  element={item.element}
                  id={item.id}
                  label={item.label}
                  maxLength={item.maxLength}
                  onChange={setInputValue}
                  onError={setInputError}
                  placeholder={item.placeholder}
                  requerid={item.requerid}
                  type={item.type}
                  value={getInputValue(item.id)}
                />
              ))}

              <button className="btn btn-big btn-primary" type="submit">
                REGISTRARME
              </button>
            </form>
          </div>
        </div>

        <br />
        <br />
      </div>
    </>
  );
};

export { LandingContainer };
