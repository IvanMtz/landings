import React from 'react';

const PolicyContainer = () => (
  <div className="policy-modal-width font-14">
    <p>
      La agencia de marketing 5Merca.com es el responsable del uso y protección
      de sus datos personales.
    </p>

    <br />

    <span className="text-bold">
      ¿Para qué fines utilizaremos sus datos personales?
    </span>

    <p>
      Los datos personales que recabamos de usted, los utilizaremos para las
      siguientes finalidades que son necesarias para el servicio que solicita:
      Comunicación personal para atención de solicitudes de apoyo.
    </p>

    <br />

    <p>
      De manera adicional, utilizaremos su información personal para las
      siguientes finalidades secundarias que
      <span className="text-bold"> no son necesarias</span>
      para el servicio solicitado, pero que nos permiten y facilitan
      brindarle una mejor atención: <br />
      - Envío de información valiosa referente a sus consultas
      en nuestros sitios<br />
      - Mercadotecnia o publicitaria <br />
      - Prospección <br />
      En caso de que no desee que sus datos personales se utilicen
      para estos fines secundarios envíe un correo a info@5merca.com
    </p>

    <br />

    <span className="text-bold">
      ¿Qué datos personales utilizaremos para estos fines?
    </span>

    <p>
      Para llevar a cabo las finalidades descritas en el presente
      aviso de privacidad, utilizaremos los siguientes datos personales: <br />
      - Nombre <br />
      - Domicilio <br />
      - Teléfono celular <br />
      - Correo electrónico <br />
      - Datos de contacto <br />
    </p>

    <br />

    <span className="text-bold">
      El uso de tecnologías de rastreo en nuestro portal de internet
    </span>

    <p>
      Le informamos que en nuestra página de internet utilizamos cookies,
      web beacons u otras tecnologías, a través de las cuales es posible
      monitorear su comportamiento como usuario de internet, así como
      brindarle un mejor servicio y experiencia al navegar en nuestra página.
      Los datos personales que obtenemos de estas tecnologías de rastreo
      son los siguientes: <br />
      - Región en la que se encuentra el usuario <br />
      - Tipo de navegador del usuario <br />
      - Tipo de sistema operativo del usuario <br />
      - Páginas web visitadas por un usuario <br />
    </p>

    <br />

    <span className="text-bold">
      ¿Cómo puede acceder, rectificar o cancelar sus datos
      personales, u oponerse a su uso?
    </span>

    <p>
      Usted tiene derecho a conocer qué datos personales tenemos
      de usted, para qué los utilizamos y las condiciones del uso que
      les damos (Acceso). Asimismo, es su derecho solicitar la
      corrección de su información personal en caso de que esté
      desactualizada, sea inexacta o incompleta (Rectificación);
      que la eliminemos de nuestros registros o bases de datos
      cuando considere que la misma no está siendo utilizada
      adecuadamente (Cancelación); así como oponerse al uso de sus
      datos personales para fines específicos (Oposición). Estos
      derechos se conocen como derechos ARCO. Para el ejercicio de
      cualquiera de los derechos ARCO, usted deberá presentar la
      solicitud respectiva a través del siguiente medio:
      info@5merca.com <br />

      Para conocer el procedimiento y requisitos para el ejercicio
      de los derechos ARCO, ponemos a su disposición el siguiente
      medio: info@5merca.com
    </p>
  </div>
);

export { PolicyContainer };