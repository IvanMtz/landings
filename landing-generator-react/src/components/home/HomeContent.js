import React, { useState, useEffect, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faTrash, faEye } from '@fortawesome/free-solid-svg-icons';

import { getLandingList, deleteLandingById } from '../../services';

import './HomeContent.css';

let idToDelete = '';
const HomeContent = () => {
  const histrory = useHistory();
  const [data, setData] = useState([]);

  const OnGetLandingList = useCallback((response) => {
    if (response.isSuccess) {
      setData(response.data);
    }
  }, [setData]);

  useEffect(() => {
    getLandingList(OnGetLandingList);
  }, [OnGetLandingList]);

  function OnDeleteLanding(response) {
    if (response.isSuccess) {
      setData(data.filter((item) => item.id !== idToDelete));
      idToDelete = '';
    }
  }

  function onCreateLanding() {
    histrory.push('/create-landing');
  }

  const onDeleteLanding = (id) => () => {
    idToDelete = id;
    deleteLandingById({ callback: OnDeleteLanding, id });
  };

  const ButtonTableAction = ({ item }) => (
    <div className="row right">
      <a className="btn btn-small btn-icon" href={`landing${item.url}`} target="_bank">
        <FontAwesomeIcon className="text-primary" icon={faEye} /> Ver
      </a>

      <button
        className="btn btn-small btn-icon"
        onClick={onDeleteLanding(item.id)}
        type="button"
      >
        <FontAwesomeIcon className="text-primary" icon={faTrash} /> Borrar
      </button>
    </div>
  );

  const ListItem = ({ index, isHeader, item }) => (
    <tr className={`row ${index % 2 === 1 ? 'grey' : 'white'}`}>
      <td className="col center no-mobile">
        <span>{item.id}</span>
      </td>

      <td className="col center">
        <span>{item.name}</span>
      </td>

      <td className="col center">
        <span>{item.url}</span>
      </td>

      <td className="col">
        {isHeader
          ? (
            <div className="col center">
              <span>Acciones</span>
            </div>
          )
          : (
            <ButtonTableAction item={item} />
          )}
      </td>
    </tr>
  );

  function renderList() {
    return (
      <table>
        <thead>
          <ListItem
            index={1}
            isHeader
            item={{ id: 'ID', name: 'Nombre', url: 'Url' }}
          />
        </thead>

        <tbody>
          {data.map((item, index) => (
            <ListItem
              key={String(index)}
              index={index}
              item={item}
            />
          ))}
        </tbody>
      </table>
    );
  }

  return (
    <>
      <div className="right">
        <button className="btn" onClick={onCreateLanding} type="button">
          <FontAwesomeIcon className="text-primary" icon={faPlus} /> Crear nuevo
        </button>
      </div>

      <h3>Listado de landings</h3>

      <div className="home-result-list-content">
        {renderList()}
      </div>
    </>
  );
};

export { HomeContent };
