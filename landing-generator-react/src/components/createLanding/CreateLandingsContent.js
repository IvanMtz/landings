import React, { useRef, useState } from 'react';
import { useHistory } from 'react-router-dom';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronCircleLeft, faWindowMinimize } from '@fortawesome/free-solid-svg-icons';

import { Input, Loading, ModalContainer } from '../common';
import {
  DESCRIPTION,
  INPUT_DEFAULT,
  INPUT_DESCRIBE_YOUR_CASE,
  INPUT_EMAIL,
  INPUT_NAME,
  INPUT_PHONE,
  INPUT_ZIPCODE,
  THANKS_SUB_TIELE,
  THANKS_TITLE,
} from './defaults';

import { saveLandingList } from '../../services';

import { toBase64 } from '../../utils';

const toolbarOptions = [
  ['bold', 'italic', 'underline'], // toggled buttons
  [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
  [{ 'color': [] }], // dropdown with defaults from theme
  [{ 'list': 'ordered' }, { 'list': 'bullet' }],
  ['clean'] // remove formatting button
];

const URL_YOUTUBE_EMBED = 'https://www.youtube.com/embed/';
let index = 0;

const CreateLandingsContent = () => {
  const history = useHistory();
  const [textRich, setTextRich] = useState('');
  const [newInput, setNewInput] = useState('');
  const [inputs, setInputs] = useState([]);
  const [describeYourCase, setDescribeYourCase] = useState(true);
  const [nameCheck, setNameCheck] = useState(true);
  const [emailCheck, setEmailCheck] = useState(true);
  const [phoneCheck, setPhoneCheck] = useState(true);
  const [zipcideCheck, setZipcodeCheck] = useState(true);
  const [loading, setLoading] = useState(false);
  const [landingName, setLandingName] = useState('');
  const [landingUrl, setLandingUrl] = useState('');
  const [hook, sethook] = useState('');
  const [videoUrl, setVideoUrl] = useState('');
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState(DESCRIPTION);
  const [thanksTitle, setThanksTitle] = useState(THANKS_TITLE);
  const [thanksSubTitle, setThanksSubtitle] = useState(THANKS_SUB_TIELE);
  const [error, setError] = useState('');

  const hookRef = useRef(null);
  const nombreRef = useRef(null);
  const urlRef = useRef(null);

  function formatUrl() {
    return `/${landingUrl.replace(/ /g, '-')}`;
  }

  function OnSaveLanding(response) {
    setLoading(false);
    const url = response.isSuccess
      ? `/landing/${formatUrl()}` : '404';

    window.open(url, '_blank');
    history.goBack();
  }

  const handleInputChange = (callback) => (event) => {
    const { target } = event;

    callback(target.value);
  };

  const handleCheckChange = ({ callback, value }) => () => callback(value);

  function onAddInput() {
    if (newInput) {
      setInputs([
        ...inputs,
        {
          ...INPUT_DEFAULT,
          id: `campo${index}`,
          placeholder: newInput,
          requerid: false,
        },
      ]);

      index += 1;
      setNewInput('');
    }
  }

  const onRemoveInput = (id) => () => {
    setInputs(inputs.filter((item) => item.id !== id));
  };

  function getData() {
    const defaultInput = [];

    if (describeYourCase) {
      defaultInput.push(INPUT_DESCRIBE_YOUR_CASE);
    }

    if (nameCheck) {
      defaultInput.push(INPUT_NAME);
    }

    if (emailCheck) {
      defaultInput.push(INPUT_EMAIL);
    }

    if (phoneCheck) {
      defaultInput.push(INPUT_PHONE);
    }

    if (zipcideCheck) {
      defaultInput.push(INPUT_ZIPCODE);
    }

    const videUrlParts = videoUrl.split('v=');
    const videoUrlId = videUrlParts.length > 1 ? videUrlParts[1] : '';

    const dataInput = {
      description,
      email: textRich,
      hook,
      inputs: [...defaultInput, ...inputs],
      name: landingName,
      thanksSubTitle,
      thanksTitle,
      title,
      url: formatUrl(),
      videoUrl: videoUrlId ? `${URL_YOUTUBE_EMBED}${videoUrlId}` : '',
    };

    return dataInput;
  }

  function onErrorFields() {
    setError('Revisa los campos requeridos');

    nombreRef.current.focus();
    nombreRef.current.blur();
    urlRef.current.focus();
    urlRef.current.blur();
    hookRef.current.focus();
    hookRef.current.blur();
  }

  function onGuardarForm() {
    if (!landingName || !landingUrl || !hook) {
      onErrorFields();

      return;
    }

    setError('');

    const data = getData();
    setLoading(true);

    saveLandingList({ callback: OnSaveLanding, data });
  }

  function onTestForm() {
    if (!landingName || !landingUrl || !hook) {
      onErrorFields();
      return;
    }

    setError('');

    const base64 = toBase64(getData());

    const { origin } = window.location;
    window.open(`${origin}/landing-test?data=${base64}`);
  }

  function onBackPage() {
    history.goBack();
  }

  const NewInputItem = ({ id, placeholder }) => (
    <div className="card-flat row grey">
      <div className="col center">
        <div className="row create-landing-row-new-input">
          <span>{`{${id}}`}</span>

          <span>{placeholder}</span>
        </div>
      </div>

      <div className="col">
        <div className="right">
          <button
            className="btn btn-small btn-colored"
            onClick={onRemoveInput(id)}
            type="button"
          >
            x
          </button>
        </div>
      </div>
    </div>
  );

  return (
    <>
      <ModalContainer show={loading} title="Guardando...">
        <Loading />
      </ModalContainer>

      <button className="btn btn-icon" onClick={onBackPage}>
        <FontAwesomeIcon className="text-primary" icon={faChevronCircleLeft} />

        <span>Regresar</span>
      </button>

      <div className="card col left">
        <h3>Campos de configuración</h3>

        <Input
          id="inpLandingName"
          inputRef={nombreRef}
          onChange={handleInputChange(setLandingName)}
          placeholder="Nombre del landing"
          requerid
          value={landingName}
        />

        <Input
          id="inpLandingUrl"
          inputRef={urlRef}
          onChange={handleInputChange(setLandingUrl)}
          placeholder="Url del landing"
          requerid
          value={landingUrl}
        />

        <Input
          id="inpLandingHook"
          inputRef={hookRef}
          onChange={handleInputChange(sethook)}
          placeholder="Hook"
          requerid
          value={hook}
        />

        <Input
          id="inpLandingUrlvideo"
          onChange={handleInputChange(setVideoUrl)}
          placeholder="https://www.youtube.com/watch?v=Dh80QyQHfDg"
          value={videoUrl}
        />

        <Input
          id="inpLandingTitle"
          onChange={handleInputChange(setTitle)}
          placeholder="Titulo"
          requerid
          value={title}
        />

        <ReactQuill
          id="description"
          className="full-width table-text-rich"
          onChange={setDescription}
          theme="snow"
          modules={{ toolbar: toolbarOptions }}
          defaultValue={{
            ops: [
              { insert: "White", attributes: { color: '#fff' } },
            ]
          }}
          value={description}
        />
      </div>

      <div className="card col left">
        <h3>Campos requeridos</h3>

        <div className="col field-requerid-container">
          <label className="checkbox" htmlFor="inpDescribeYourCase">
            <div className="row">
              <span className="col">{'{tuCaso}'}</span>

              <span className="col">Describe tu caso</span>
            </div>

            <input
              checked={describeYourCase}
              id="inpDescribeYourCase"
              onChange={handleCheckChange({
                callback: setDescribeYourCase,
                value: !describeYourCase,
              })}
              type="checkbox"
            />
          </label>

          <label className="checkbox" htmlFor="inpName">
            <div className="row">
              <span className="col">{'{nombre}'}</span>

              <span className="col">Nombre</span>
            </div>

            <input
              checked={nameCheck}
              id="inpName"
              onChange={handleCheckChange({
                callback: setNameCheck,
                value: !nameCheck,
              })}
              type="checkbox"
            />
          </label>

          <label className="checkbox" htmlFor="inpEmail">
            <div className="row">
              <span className="col">{'{correo} '}</span>

              <span className="col">Correo</span>
            </div>

            <input
              checked={emailCheck}
              id="inpEmail"
              onChange={handleCheckChange({
                callback: setEmailCheck,
                value: !emailCheck,
              })}
              type="checkbox"
            />
          </label>

          <label className="checkbox" htmlFor="inpPhone">
            <div className="row">
              <span className="col">{'{telefono}'}</span>

              <span className="col">Télefono</span>
            </div>

            <input
              checked={phoneCheck}
              id="inpPhone"
              onChange={handleCheckChange({
                callback: setPhoneCheck,
                value: !phoneCheck,
              })}
              type="checkbox"
            />
          </label>

          <label className="checkbox" htmlFor="inpZipCode">
            <div className="row">
              <span className="col">{'{cp}'}</span>

              <span className="col">C.P.</span>
            </div>

            <input
              checked={zipcideCheck}
              id="inpZipCode"
              onChange={handleCheckChange({
                callback: setZipcodeCheck,
                value: !zipcideCheck,
              })}
              type="checkbox"
            />
          </label>
        </div>
      </div>

      <div className="col card">
        <h3>Campos adicionales</h3>

        <div className="row">
          <Input
            id="newInput"
            onChange={handleInputChange(setNewInput)}
            placeholder="Nombre del campo"
            value={newInput}
          />

          <button
            className="btn btn-colored btn-add-input"
            onClick={onAddInput}
            type="button"
          >
            Agregar
          </button>
        </div>

        <div className="col">
          {inputs.map((item, i) => (
            <NewInputItem key={String(i)} {...item} />
          ))}
        </div>
      </div>

      <div className="col card">
        <h3>Campos pagina gracias</h3>

        <Input
          id="inpLandingThanksTitle"
          onChange={handleInputChange(setThanksTitle)}
          placeholder={THANKS_TITLE}
          requerid
          value={thanksTitle}
        />

        <Input
          id="inpLandingThanksSubTitle"
          onChange={handleInputChange(setThanksSubtitle)}
          placeholder={THANKS_SUB_TIELE}
          requerid
          value={thanksSubTitle}
        />

      </div>

      <div className="card col left">
        <h3>Correo autómatico</h3>

        <ReactQuill
          className="full-width table-text-rich"
          onChange={setTextRich}
          theme="snow"
          value={textRich}
        />
      </div>

      <div className="col">
        <div className="row right">
          <span className="text-error">{error}</span>
        </div>


        <div className="row right button-create-landings">
          <button
            className="btn btn-colored"
            onClick={onBackPage}
            type="button"
          >
            Cancelar
          </button>

          <button
            className="btn btn-colored"
            onClick={onTestForm}
            type="button"
          >
            Enviar prueba
          </button>

          <button
            className="btn btn-primary"
            onClick={onGuardarForm}
            type="button"
          >
            Guardar
          </button>

        </div>
      </div>
    </>
  );
};

export { CreateLandingsContent };
