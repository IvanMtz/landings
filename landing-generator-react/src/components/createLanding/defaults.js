export const INPUT_DEFAULT = {
  id: '',
  placeholder: '',
  requerid: true,
  type: 'text',
  value: '',
};

export const INPUT_DESCRIBE_YOUR_CASE = {
  ...INPUT_DEFAULT,
  id: 'describeTuCaso',
  placeholder: 'Describe tu caso',
  label: 'Describe tu caso',
  element: 'textarea'
};

export const INPUT_NAME = {
  ...INPUT_DEFAULT,
  id: 'nombre',
  placeholder: 'Nombre',
};

export const INPUT_EMAIL = {
  ...INPUT_DEFAULT,
  id: 'correo',
  placeholder: 'Correo',
  type: 'email',
};

export const INPUT_PHONE = {
  ...INPUT_DEFAULT,
  id: 'telefono',
  maxLength: 10,
  placeholder: 'Télefono',
  type: 'number',
};

export const INPUT_ZIPCODE = {
  ...INPUT_DEFAULT,
  id: 'cp',
  maxLength: 5,
  placeholder: 'C.P.',
  type: 'number',
};

/* eslint-disable max-len */
export const DESCRIPTION = 'Antes de considerarme político, me considero un ciudadano que ha padecido y padece las dificultades sociales, por eso trabajo de la mano de ustedes, estoy para servirte';

export const THANKS_TITLE = '¡Gracias!';

/* eslint-disable max-len */
export const THANKS_SUB_TIELE = 'Nos comunicaremos contigo para darle seguimiento a tu solicitud';
