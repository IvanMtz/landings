import React from 'react';

import jesusNava from '../../assets/jesus-nava.png';
import { WhatsappContact } from '../landing/WhatsappContact';


const LandingsThanksContainer = ({ data }) => {
  return (
    <div className="primary-color-dark full-height">
      <div className="container center col">
        <img className="thank-img-jesus-nava" src={jesusNava} />

        <div className="thank-title-container col text-center center">
          <h1 className="text-blue">{data.thanksTitle}</h1>
        </div>

        <p className="text-white thank-sub-title text-bold text-center">
          {data.thanksSubTitle}
        </p>

        <WhatsappContact />
      </div>
    </div>
  );
};

export { LandingsThanksContainer };