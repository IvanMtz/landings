import React from 'react';
import './modalContainer.css';

const ModalContainer = ({
  handleOutClick,
  show,
  title,
  ...rest
}) => (
  <>
    {show
      && (
        <div
          className="modal-container"
          id="modalOuter"
          onClick={handleOutClick}
          onKeyDown={handleOutClick}
          onKeyPress={handleOutClick}
          role="button"
          tabIndex={0}
        >
          <div className="card-container">
            <div
              className={`modal-card-title-header ${!title && 'Gone'}`}
            >
              <span className="modal-card-title">{title}</span>
            </div>

            {rest.children}
          </div>
        </div>
      )}
  </>
);

export { ModalContainer };
