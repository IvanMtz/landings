import React, { useState } from 'react';

import { isValidEmail } from '../../utils';

import './input.css';

const InputNoMemo = ({
  element = 'input',
  id,
  inputRef,
  label = '',
  maxLength,
  onBlur,
  onChange,
  onError,
  onFocus,
  placeholder,
  requerid = false,
  type = 'text',
  value,
}) => {
  const [error, setError] = useState('');

  function handleOnChange(event) {
    if (onChange) {
      onChange(event);
    }
  }

  function handleOnBlur(event) {
    const { target } = event;

    if (!value && requerid) {
      setError('Campo requerido');

      if (onError) {
        onError({ error: 'Campo requerido', id });
      }
    } else if (type === 'email' && !isValidEmail(target.value)) {
      setError('Formato de email incorrecto.');

      if (onError) {
        onError({ error: 'Formato de email incorrecto.', id });
      }
    }

    if (onBlur) {
      onBlur(event);
    }
  }

  function handleOnFocus(event) {
    setError('');

    if (onError) {
      onError({ error: '', id });
    }

    if (onFocus) {
      onFocus(event);
    }
  }

  function renderElement() {
    switch (element) {
      case 'textarea':
        return (
          <textarea
            className={`text-area-from input-from `
              + `${error ? 'input-from-error' : ''}`}
            id={id}
            maxLength={maxLength}
            onBlur={handleOnBlur}
            onChange={handleOnChange}
            onFocus={handleOnFocus}
            placeholder={placeholder}
            required={requerid}
            type={type}
            value={value}
          />
        );

      default:
        return (
          <input
            className={`input-from ${error ? 'input-from-error' : ''}`}
            id={id}
            maxLength={maxLength}
            onBlur={handleOnBlur}
            onChange={handleOnChange}
            onFocus={handleOnFocus}
            placeholder={placeholder}
            ref={inputRef}
            required={requerid}
            type={type}
            value={value}
          />
        );
    }
  }

  return (
    <div className="col input-from-container">
      {label ? <span className="input-from-label">{label}</span> : null}

      {renderElement()}

      <span className="text-error input-from-error-label">{error}</span>
    </div>
  );
};

const Input = React.memo(InputNoMemo);

export { Input };
