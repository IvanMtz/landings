import React, { useContext } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';

import { Context } from '../../Context';

import logoBlanco from '../../assets/logo-nava-blanco.png';

import './header.css';

const Header = () => {
  const { removeAuth } = useContext(Context);

  function onLogOut() {
    removeAuth();
  }

  return (
    <header className="header-container">
      <div className="row header-logo">
        <img alt="logo-jesus-nava" className="header-logo" src={logoBlanco} />

        <h1 className="no-mobile">Creador de landings</h1>
      </div>

      <button
        className="btn header-button btn-icon"
        onClick={onLogOut}
        type="button"
      >
        <FontAwesomeIcon className="text-primary" icon={faSignOutAlt} />  Salir
      </button>
    </header>
  );
};

export { Header };
