import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';

import { Context } from '../../Context';
import { Input, Loading, ModalContainer } from '../common';

import { login } from '../../services';
import logoLogin from '../../assets/logo-login.png';

import './LoginFrom.css';

const LoginFrom = () => {
  const history = useHistory();
  const { activateAuth } = useContext(Context);
  const [user, setUser] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  function onLoginComplete(response) {
    setLoading(false);

    if (response.isSuccess) {
      const { accessToken } = response.data;

      activateAuth(accessToken);
      history.push('/home');
    } else {
      setError('Usuario y/o contraseña incorrecta.')
    }
  }

  function handleSutmit(event) {
    event.preventDefault();

    if (user && password) {
      setLoading(true);
      setError('');

      const params = { email: user, password };
      login({ callback: onLoginComplete, params });
    }
  }

  const handleInputChange = (callback) => (event) => {
    const { target } = event;

    callback(target.value);
  };

  return (
    <>
      <ModalContainer show={loading} title="Cargando...">
        <Loading />
      </ModalContainer>

      <div className="row center">
        <div className="card center login-container">
          {/* <div className="login-container-left">

          </div> */}
          <div className="col">
            <div className="login-logo-container">
              <img className="login-form-logo" src={logoLogin} />
            </div>

            <form className="col" onSubmit={handleSutmit}>
              <Input
                onChange={handleInputChange(setUser)}
                placeholder="Email"
                requerid
                type="email"
                value={user}
              />

              <Input
                onChange={handleInputChange(setPassword)}
                placeholder="Contraseña"
                requerid
                type="password"
                value={password}
              />

              <button
                className="btn btn-primary"
                onClick={handleSutmit}
                onSubmit={handleSutmit}
                type="submit"
              >
                Entrar
              </button>

              <span className="text-error">{error}</span>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export { LoginFrom };
