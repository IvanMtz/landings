import React, { useState } from 'react';
import { getToken } from './utils';

export const Context = React.createContext();

const Provider = ({ children }) => {
  const [isAuth, setIsAuth] =
    useState(() => getToken());

  const value = {
    activateAuth: (token) => {
      setIsAuth(true);
      window.sessionStorage.setItem('token', token);
    },
    isAuth,
    removeAuth: () => {
      setIsAuth(false);
      window.sessionStorage.removeItem('token');
    },
  };

  return (
    <Context.Provider value={value}>
      {children}
    </Context.Provider>
  );
};

export default { Consumer: Context.Consumer, Provider };
