const env = process.env.NODE_ENV === 'development' 
? 'http://localhost:3600' : 'http://161.35.5.78:3600';

export { env };
