export function setSessionStorage({ data, key }) {
  if (data && key) {
    sessionStorage.setItem(key, JSON.stringify(data));
  }
}

export function getSessionStorage(key) {
  const data = sessionStorage.getItem(key);

  if (data) {
    return JSON.parse(data);
  }

  return null;
}

export function removeSessionStorage(key) {
  sessionStorage.removeItem(key);
}

export function getToken() {
  return window.sessionStorage.getItem('token');
}
