import { getToken } from '.';
import { env } from '../env';

const BASE_IP = env;

let Context = null;

// serializeForUri({ param0: 'value 0', param1: 'value:1' }) ===
// 'param0=value%200&param1=value%3A1'
function serializeForUri(obj = {}) {
  return Object
    .keys(obj)
    .map((key) => `${encodeURI(key)}=${encodeURI(obj[key])}`)
    .join('&');
}

export function setContext(context) {
  Context = context;
}

function logOut() {
  if (Context) {
    Context.removeAuth();
  }
}

async function call(URL, data, method) {
  const token = await getToken('token');

  const headers = {
    authorization: `Bearer ${token}`,
    'Content-Type': 'application/json',
  };

  try {
    const controller = new AbortController();

    const options = {
      body: data && JSON.stringify(data),
      headers,
      method,
      signal: controller.signal,
    };
    const timeId = setTimeout(() => controller.abort(), 8000);
    const request = await fetch(`${BASE_IP}/${URL}`, options);

    clearTimeout(timeId);

    if (token && request.status === 401) {
      logOut();
      throw new Error('401');
    }

    // no content
    if (request.status === 204) {
      return { isSuccess: true };
    }

    const response = await request.json();
    return { data: response, isSuccess: request.ok };
  } catch (error) {
    return { error, isSuccess: false };
  }
}

export function get(URL, params = {}) {
  const queryParams = serializeForUri(params);
  const url = `${URL}?${queryParams}`;

  return call(url, null, 'GET');
}

export function post(URL, data, formData = false) {
  return call(URL, data, 'POST', formData);
}

export function PUT(URL, data, formData = false) {
  return call(URL, data, 'PUT', formData);
}

export function del(URL) {
  return call(URL, null, 'DELETE');
}
