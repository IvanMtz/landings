export function isValidEmail(email) {
  const regexString = '^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@'
    + '[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*\\.[a-zA-Z]{2,4}$';
  const regex = new RegExp(regexString);

  return regex.test(String(email).toLowerCase());
}

export function toBase64(data) {
  const queryData = JSON.stringify(data);
  const base64 = window.btoa(queryData);

  return base64;
}

export function fromBase64(stringBase64) {
  if (!stringBase64) {
    return {};
  }

  const data = window.atob(stringBase64);

  return data;
}

export function dataFromUrlBase64(search) {
  const stringBase64 = search.replace('?data=', '');

  return fromBase64(stringBase64);
}